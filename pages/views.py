from django.shortcuts import render
from django.http import HttpResponse

from products.models import Product
from sellers.models import Seller


def index(request):
    products = Product.objects.order_by('-id').filter(is_published=True)[:3]

    context = {
        'products': products,
    }
    return render(request, 'pages/index.html', context)


def about(request):
    return render(request, 'pages/about.html',)
